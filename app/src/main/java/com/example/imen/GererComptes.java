package com.example.imen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class GererComptes extends AppCompatActivity {
    private static final String URL_gerer_Comptes="http://192.168.1.7/imen/gerer_compte.php";

    private EditText name;
    private EditText firstname;
    private EditText emailAddress;
    private EditText date;

    private RadioButton homme;

    private RadioButton femme;

    private ImageButton addButton;
    private ImageButton deleteButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gerer_comptes);

        name = (EditText) findViewById(R.id.editTextName);
        firstname = (EditText) findViewById(R.id.firstname);
        emailAddress = (EditText) findViewById(R.id.editTextTextEmailAddress);
        date = (EditText) findViewById(R.id.date);
        homme = (RadioButton) findViewById(R.id.homme);
        femme = (RadioButton) findViewById(R.id.femme);

        addButton = (ImageButton) findViewById(R.id.addButton);
        deleteButton = (ImageButton) findViewById(R.id.deleteButton);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertInfo();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteInfo();
            }
        });
    }

    private void insertInfo() {
        String nameStr = name.getText().toString();
        String firstnameStr = firstname.getText().toString();
        String birthdayStr = date.getText().toString();
        String emailStr = emailAddress.getText().toString();
        String genreStr = homme.isChecked() ? "Homme" : "Femme";

        StringRequest stringRequest= new StringRequest(Request.Method.GET, URL_gerer_Comptes + "?Nom=" + nameStr + "&Prenom=" + firstnameStr + "&Date_Naissance=" + birthdayStr + "&email=" + emailStr + "&Genere=" + genreStr, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String success = jsonObject.getString("success");

                    if (success.equals("1")){
                        Toast.makeText(GererComptes.this, "Opération effectué avec succès", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(GererComptes.this,"ERROR1"+e.toString(),Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(GererComptes.this,"ERROR"+error.toString(),Toast.LENGTH_LONG).show();
            }
        });

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void deleteInfo() {
        String emailStr = emailAddress.getText().toString();

        StringRequest stringRequest= new StringRequest(Request.Method.GET, URL_gerer_Comptes + "?delete=true&email=" + emailStr, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String success = jsonObject.getString("success");

                    if (success.equals("1")){
                        Toast.makeText(GererComptes.this, "Le compte a été supprimé avec succès", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(GererComptes.this,"ERROR1"+e.toString(),Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(GererComptes.this,"ERROR"+error.toString(),Toast.LENGTH_LONG).show();
            }
        });

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}