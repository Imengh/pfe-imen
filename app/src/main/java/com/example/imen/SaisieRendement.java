package com.example.imen;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import modele.Chaine;
import modele.Employe;
import modele.HoraireTravail;
import modele.Operation;
import modele.PlageHoraire;
import modele.Ticket;

public class SaisieRendement extends AppCompatActivity {

    Employe employe = null;
    Chaine chaine = null;
    PlageHoraire plageHoraire = null;
    Ticket ticket = null;
    Operation operation = null;

    private Spinner spinnerWorkChain;
    private Spinner spinnerHours;

    private EditText durationInMinutes;
    private EditText durationLabel;
    private EditText suiveuse;
    private EditText rendement;
    private EditText cumulMinutesDeTravail;
    private EditText quantity;
    private EditText nombre_code_barre_acceeptees_employe;


    private static final String URL_liste_chaine="http://192.168.1.7/imen/rendement.php";
    private static final String URL_employee="http://192.168.1.7/imen/employee.php";
    private static final String URL_ticket = "http://192.168.1.7/imen/ticket.php";
    private static final String URL_calculer_rendement = "http://192.168.1.7/imen/Calculer_rendement.php";
    private static final String URL_horaire_travail="http://192.168.1.7/imen/horaire_travail.php";
    private static final String URL_quantity="http://192.168.1.7/imen/quantity.php";
    private static final String URL_nbrecodebare="http://192.168.1.7/imen/nombre_code_barre_acceeptees_employe.php";
    private List<String> durationList = new ArrayList<>();
    private Map<String, String> employee = new HashMap<>();
    private EditText registration;
    private EditText name;

    private Button btnCalcul;
    private Button btnOperation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saisie_rendement);

        spinnerWorkChain = (Spinner) findViewById(R.id.spinnerWorkChain);
        spinnerHours = (Spinner) findViewById(R.id.spinnerHours);
        registration = (EditText) findViewById(R.id.registration);
        suiveuse = (EditText) findViewById(R.id.suiveuse);
        rendement = (EditText) findViewById(R.id.rendement);
        quantity = (EditText) findViewById(R.id.quantity);
        nombre_code_barre_acceeptees_employe = (EditText) findViewById(R.id.nombre_code_barre_acceeptees_employe);

        durationInMinutes = (EditText) findViewById(R.id.durationInMinutes);
        durationLabel = (EditText) findViewById(R.id.durationLabel);
        name = (EditText) findViewById(R.id.name);
        cumulMinutesDeTravail = (EditText) findViewById(R.id.cumulMinutesDeTravail);

        btnCalcul = (Button) findViewById(R.id.btncalcul);
        btnOperation = (Button) findViewById(R.id.btnoperation);

        this.getChainList();

        this.suiveuse.setEnabled(false);

        suiveuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(suiveuse.isEnabled()) {
                    IntentIntegrator scanIntegrator = new IntentIntegrator(SaisieRendement.this);
                    scanIntegrator.initiateScan();
                }
            }
        });

        spinnerHours.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                durationInMinutes.setText(durationList.get(position));
                String durationStr = parseDurationLabel(durationList.get(position));

                durationLabel.setText(durationStr);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        registration.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().length() == 0) {
                    name.setText("");
                } else {
                    getEmployeeInformation(s + "");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // Lancer les opérations dans un nouveau Thread (pour ne pas bloquer le Thread UI principale)
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    if(spinnerHours.getSelectedItem() != null && name.getText().length() > 0 &&
                            spinnerWorkChain.getSelectedItem() != null) {

                        //execute une opération dans le Thread principale
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                suiveuse.setEnabled(true);
                            }
                        });
                    } else {
                        //execute une opération dans le Thread principale
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                suiveuse.setEnabled(false);
                            }
                        });
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        suiveuse.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId){
                    case EditorInfo.IME_ACTION_DONE:
                    case EditorInfo.IME_ACTION_NEXT:
                    case EditorInfo.IME_ACTION_PREVIOUS:
                        reserverTicket();
                        nombre_code_barre_acceeptees_employe();
                        return true;
                }
                return false;
            }
        });

        btnCalcul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculerRendement();
            }
        });

        btnOperation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String suiveuseStr = suiveuse.getText().toString();
                getQuantityByTicket(suiveuseStr);
            }
        });

        calculerCumul();
    }

    private String parseDurationLabel(String durationInMinutes) {
        String result = "";

        int minutes = Integer.parseInt(durationInMinutes);
        int hours = minutes / 60 ;
        minutes = minutes % 60;

        result += (hours > 0) ? hours + "Heures " : "";
        result += (minutes > 0) ? minutes + "Minutes " : "";

        return result;
    }

    private void getEmployeeInformation(String searchValue) {
        if(searchValue.length() >= 3){
            StringRequest stringRequest= new StringRequest(Request.Method.GET, URL_employee + "?registration=" + searchValue, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");
                        String employeeName = jsonObject.getString("Name");
                        employe = new Employe(searchValue, employeeName);

                        if (success.equals("1")){
                            name.setText(employe.getNom());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(SaisieRendement.this,"ERROR1"+e.toString(),Toast.LENGTH_SHORT).show();
                    }
                }
            },new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(SaisieRendement.this,"ERROR"+error.toString(),Toast.LENGTH_LONG).show();
                }
            });

            RequestQueue requestQueue= Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }

    private void getChainList() {
        StringRequest stringRequest= new StringRequest(Request.Method.GET, URL_liste_chaine, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String success = jsonObject.getString("success");
                    JSONArray chainListIds = jsonObject.getJSONArray("chainList");
                    JSONArray hoursLibs = jsonObject.getJSONArray("hours");
                    JSONArray durations = jsonObject.getJSONArray("durations");

                    List<String> spinnerWorkChainArray =  new ArrayList<String>();
                    //Création d'un adaptateur pour adapter la liste( la chaine de caractere) à notre spinner
                    ArrayAdapter<String> adapterWorkChain = new ArrayAdapter<String>(
                            SaisieRendement.this, android.R.layout.simple_spinner_dropdown_item, spinnerWorkChainArray);
                    adapterWorkChain.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    List<String> spinnerHoursArray =  new ArrayList<String>();
                    ArrayAdapter<String> adapterHours = new ArrayAdapter<String>(
                            SaisieRendement.this, android.R.layout.simple_spinner_dropdown_item, spinnerHoursArray);
                    adapterHours.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    if (success.equals("1")){
                        for (int i=0; i< chainListIds.length(); i++){
                            spinnerWorkChainArray.add(chainListIds.getString(i));
                        }

                        for (int i=0; i< hoursLibs.length(); i++){
                            spinnerHoursArray.add(hoursLibs.getString(i));
                        }

                        for (int i=0; i< durations.length(); i++){
                            durationList.add(durations.getString(i));
                        }

                        spinnerWorkChain.setAdapter(adapterWorkChain);
                        spinnerHours.setAdapter(adapterHours);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SaisieRendement.this,"ERROR1"+e.toString(),Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SaisieRendement.this,"ERROR"+error.toString(),Toast.LENGTH_LONG).show();
            }
        });

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void reserverTicket() {
        String suiveuse = this.suiveuse.getText().toString();

        String empId = this.registration.getText().toString();
        if(employe != null) {
            employe.setMatricule(empId);
        }

        String chId = this.spinnerWorkChain.getSelectedItem().toString();
        chaine = new Chaine(chId);

        String PlageH_Id = this.spinnerHours.getSelectedItem().toString();
        String Duree = this.durationInMinutes.getText().toString();
        HoraireTravail horaireTravail = new HoraireTravail(Duree);
        plageHoraire = new PlageHoraire(PlageH_Id, horaireTravail);

        ticket = new Ticket(suiveuse, employe, plageHoraire, chaine);

        StringRequest stringRequest= new StringRequest(Request.Method.GET, URL_ticket + ticket.getParams(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String success = jsonObject.getString("success");
                    String message = jsonObject.getString("message");

                    if (success.equals("1")){
                        Toast.makeText(SaisieRendement.this, message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SaisieRendement.this,"ERROR1"+e.toString(),Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SaisieRendement.this,"ERROR"+error.toString(),Toast.LENGTH_LONG).show();
            }
        });

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void calculerRendement() {
        String empId = this.registration.getText().toString();
        String duration = this.durationInMinutes.getText().toString();

        StringRequest stringRequest= new StringRequest(Request.Method.GET, URL_calculer_rendement + "?registration=" + empId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String success = jsonObject.getString("success");
                    String total = jsonObject.getString("total");

                    if (success.equals("1")){
                        Float totalDecimal = Float.parseFloat(total);
                        Float durationDecimal = Float.parseFloat(duration);
                        rendement.setText(((totalDecimal * 100) / 510) + "");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SaisieRendement.this,"ERROR1"+e.toString(),Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SaisieRendement.this,"ERROR"+error.toString(),Toast.LENGTH_LONG).show();
            }
        });

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void calculerCumul() {
        StringRequest stringRequest= new StringRequest(Request.Method.GET, URL_horaire_travail, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String success = jsonObject.getString("success");
                    JSONObject result = jsonObject.getJSONObject("result");

                    String weekMinutesStr = (String) result.get("L-V");
                    String saturdayMinutesStr = (String) result.get("SAM");

                    int weekMinutes = Integer.parseInt(weekMinutesStr);
                    int saturdayMinutes = Integer.parseInt(saturdayMinutesStr);

                    if (success.equals("1")){
                        cumulMinutesDeTravail.setText((weekMinutes *5 + saturdayMinutes) + "");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SaisieRendement.this,"ERROR1"+e.toString(),Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SaisieRendement.this,"ERROR"+error.toString(),Toast.LENGTH_LONG).show();
            }
        });

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getQuantityByTicket(String searchValue) {
        if(searchValue.length() > 0){
            StringRequest stringRequest= new StringRequest(Request.Method.GET, URL_quantity + "?suiveuse=" + searchValue, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");
                        String quantityStr = jsonObject.getString("quantity");
                        operation = new Operation(quantityStr);
                        if (success.equals("1")){
                            quantity.setText(operation.getQuantite());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(SaisieRendement.this,"ERROR1"+e.toString(),Toast.LENGTH_SHORT).show();
                    }
                }
            },new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(SaisieRendement.this,"ERROR"+error.toString(),Toast.LENGTH_LONG).show();
                }
            });

            RequestQueue requestQueue= Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            String scanContent = scanningResult.getContents();
            String scanFormat = scanningResult.getFormatName();

            suiveuse.setText(scanContent);
        } else {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }


    private void nombre_code_barre_acceeptees_employe() {
        StringRequest stringRequest= new StringRequest(Request.Method.GET, URL_nbrecodebare + "?registration=" + registration.getText().toString()+"&spinnerWorkChain="+ spinnerWorkChain.getSelectedItem().toString() , new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");
                        String nombreStr = jsonObject.getString("nombre");
                        if (success.equals("1")){
                            nombre_code_barre_acceeptees_employe.setText(nombreStr);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(SaisieRendement.this,"ERROR1"+e.toString(),Toast.LENGTH_SHORT).show();
                    }
                }
            },new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(SaisieRendement.this,"ERROR"+error.toString(),Toast.LENGTH_LONG).show();
                }
            });

            RequestQueue requestQueue= Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

    }

}