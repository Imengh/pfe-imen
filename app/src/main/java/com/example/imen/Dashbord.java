package com.example.imen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Dashbord extends AppCompatActivity {

    Button gotToDeclaration;

    Button goToAccountManagement;

    Button goToQualityControl;
    Button gotToRendementParChaine;
    Button gotToRendementParEmployer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashbord);

        gotToDeclaration = (Button) findViewById(R.id.buttonProfile);
        goToAccountManagement = (Button) findViewById(R.id.goToAccountManagement);
        goToQualityControl = (Button) findViewById(R.id.buttonQualityControl);
        gotToRendementParChaine = (Button) findViewById(R.id.buttonPresence);
        gotToRendementParEmployer = (Button) findViewById(R.id.buttonNote);

        gotToDeclaration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashbord.this, SaisieRendement.class);
                startActivity(intent);
            }
        });

        goToQualityControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashbord.this, QualityControl.class);
                startActivity(intent);
            }
        });

        goToAccountManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashbord.this, GererComptes.class);
                startActivity(intent);
            }

        });
        gotToRendementParChaine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashbord.this, RendementParChaine.class);
                startActivity(intent);
            }
        });

        gotToRendementParEmployer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashbord.this, RendementParEmployer.class);
                startActivity(intent);
            }
        });
    }

}