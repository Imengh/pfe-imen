package com.example.imen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class RendementParEmployer extends AppCompatActivity {
    private static final String URL_getRendement="http://192.168.1.7/imen/rendement_par_employer.php";
    private TextView rendement;
    private Button button;
    private EditText empId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rendement_par_employer);
        empId = findViewById(R.id.empId);
        button = findViewById(R.id.btnValider);
        rendement = findViewById(R.id.rendement);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRendement();
            }
        });
    }

    private void getRendement() {
        String empIdStr = empId.getText().toString();

        StringRequest stringRequest= new StringRequest(Request.Method.GET, URL_getRendement + "?registration=" + empIdStr , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {


                    JSONObject jsonObject = new JSONObject(response);
                    String success = jsonObject.getString("success");
                    String rendementStr = jsonObject.getString("rendement");

                    if(success.equals("1"))
                    rendement.setText(rendementStr);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(RendementParEmployer.this,"ERROR1"+e.toString(),Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RendementParEmployer.this,"ERROR"+error.toString(),Toast.LENGTH_LONG).show();
            }
        });

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}