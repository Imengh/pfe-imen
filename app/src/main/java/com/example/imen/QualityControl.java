package com.example.imen;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import modele.Commande;
import modele.Controleuse;
import modele.Operation;
import modele.Paquet;
import modele.User;

public class QualityControl extends AppCompatActivity {
    private Spinner controleuses;
    private static final String URL_controleuses = "http://192.168.1.7/imen/controleuses.php";
    private static final String URL_vTicketInfo = "http://192.168.1.7/imen/get-v-ticket-information.php";
    private static final String URL_vTicket = "http://192.168.1.7/imen/nbr_pieces_paquet.php";
    private EditText ficheSuiveuse;
    private EditText information;
    private Controleuse controleuse = null;
    private Paquet paquet = null;
    private Commande commande = null;
    private User user = null;
    private Operation operation = null;
    private EditText editTextNumberSigned;
    private EditText nbrePiecesControlees;
    private EditText nbreDefautsTrouvees;
    private ImageButton addButton;
    private ImageButton returnButton;

    private ArrayList<String> controleuseIds = new ArrayList<>();
    private String controleuseId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quality_control);
        controleuses = (Spinner) findViewById(R.id.controleuses);
        ficheSuiveuse = (EditText) findViewById(R.id.ficheSuiveuse);
        information = (EditText) findViewById(R.id.information);
        editTextNumberSigned = (EditText) findViewById(R.id.editTextNumberSigned);
        nbrePiecesControlees = (EditText) findViewById(R.id.nbrePiecesControlees);
        nbreDefautsTrouvees = (EditText) findViewById(R.id.nbreDefautsTrouvees);
        addButton = (ImageButton) findViewById(R.id.imageButton);
        returnButton = (ImageButton) findViewById(R.id.imageButton4);
        getControleusesInformation();

        ficheSuiveuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator scanIntegrator = new IntentIntegrator(QualityControl.this);
                scanIntegrator.initiateScan();
            }
        });
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {getVTicket(); }
        });
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QualityControl.this,Dashbord.class);
                startActivity(intent);
            }
        });

        ficheSuiveuse.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String value = (String) s.toString();
                if (value.length() > 1) {
                    getVTicketInfo(value);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        controleuses.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position < controleuseIds.size()){
                    controleuseId = controleuseIds.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getControleusesInformation() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_controleuses, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String success = jsonObject.getString("success");
                    JSONObject result = jsonObject.getJSONObject("result");

                    List<String> spinnerValues = new ArrayList<String>();
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                            QualityControl.this, android.R.layout.simple_spinner_dropdown_item, spinnerValues);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    if (success.equals("1")) {
                        Iterator<String> iter = result.keys();
                        while (iter.hasNext()) {
                            String key = iter.next();
                            try {
                                JSONObject value = result.getJSONObject(key);
                                String id = value.getString("id");
                                String lib = value.getString("lib");

                                spinnerValues.add(lib);
                                controleuseIds.add(id);
                            } catch (JSONException e) {
                                // Something went wrong!
                            }
                        }

                        controleuses.setAdapter(adapter);
                        controleuseId = controleuses.getSelectedItem().toString();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(QualityControl.this, "ERROR1" + e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(QualityControl.this, "ERROR" + error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void getVTicketInfo(String searchValue) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_vTicketInfo + "?suiveuse=" + searchValue, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String success = jsonObject.getString("success");
                    String operation = jsonObject.getString("Numero_Opn");
                    QualityControl.this.operation = new Operation(null, operation);
                    String cde = jsonObject.getString("Numero_Cde");
                    QualityControl.this.commande = new Commande(cde);
                    String paquet = jsonObject.getString("paquet");
                    QualityControl.this.paquet = new Paquet(paquet, null);


                    if (success.equals("1")) {
                        information.setText(QualityControl.this.operation.getNumero() + ", " + commande.getNumero()
                                + " et " + QualityControl.this.paquet.getNumero());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(QualityControl.this, "ERROR1" + e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(QualityControl.this, "ERROR" + error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            String scanContent = scanningResult.getContents();
            String scanFormat = scanningResult.getFormatName();

            ficheSuiveuse.setText(scanContent);
        } else {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }


    private void getVTicket() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_vTicket + "?CE_Pcs_Cont=" +
                editTextNumberSigned.getText().toString() + "&CE_Ctrl_Id=" + controleuseId + "&CE_Nbr_Ret=" + nbreDefautsTrouvees.getText().toString() + "&suiveuse=" + ficheSuiveuse.getText().toString() , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String success = jsonObject.getString("success");
                    String message = jsonObject.getString("message");

                    if (success.equals("1")) {
                        Toast.makeText(QualityControl.this, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(QualityControl.this, "ERROR1" + e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(QualityControl.this, "ERROR" + error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}
