package com.example.imen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import modele.Employe;


public class RendementParChaine extends AppCompatActivity {
    private static final String URL_getRendementInformation="http://192.168.1.7/imen/rendement_par_chaine.php";
    private TextView rendementCh1;
    private TextView rendementch2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rendement_par_chaine);

        rendementCh1 = (TextView) findViewById(R.id.rendementCh1);
        rendementch2 = (TextView) findViewById(R.id.rendementCh2);

        this.getRendementInformation();
    }

    private void getRendementInformation() {
            StringRequest stringRequest= new StringRequest(Request.Method.GET, URL_getRendementInformation , new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");
                        String rendement_ch1 = jsonObject.getString("rendement_ch1");
                        String rendement_ch2 = jsonObject.getString("rendement_ch2");

                        rendement_ch1 = (Float.parseFloat(rendement_ch1) / 510) + "";
                        rendement_ch2 = (Float.parseFloat(rendement_ch2) / 510) + "";
                        if(success.equals("1")) {
                            rendementCh1.setText(rendement_ch1);
                            rendementch2.setText(rendement_ch2);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(RendementParChaine.this,"ERROR1"+e.toString(),Toast.LENGTH_SHORT).show();
                    }
                }
            },new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(RendementParChaine.this,"ERROR"+error.toString(),Toast.LENGTH_LONG).show();
                }
            });

            RequestQueue requestQueue= Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }