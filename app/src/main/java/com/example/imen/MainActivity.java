package com.example.imen;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    Button btn1;
    ProgressBar Loading;
    EditText username,password;
    private static String URL_LOGIN="http://192.168.1.7/imen/login.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Loading=(ProgressBar)findViewById(R.id.progress);
        username=(EditText)findViewById(R.id.editText);
        password=(EditText)findViewById(R.id.editText2);
        btn1=(Button)findViewById(R.id.button1);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = username.getText().toString().trim();
                String pass = password.getText().toString().trim();
                if(!user.isEmpty() || !pass.isEmpty()){
                    Login(user,pass);
                }
                else {
                    username.setError("Please insert Username");
                    password.setError("Please insert password");
                }
            }
        });
    }

    private void Login(final String username , final String password) {
        Loading.setVisibility(View.VISIBLE);
        btn1.setVisibility(View.GONE);

        //StringRequest Objet pour lancer des requetes HTTP
        StringRequest stringRequest= new StringRequest(Request.Method.POST, URL_LOGIN, new Response.Listener<String>() {

            //Si le PHP fonctionne correctement on execute onResponse (avec la reponse JSON dans String response)
            @Override
            public void onResponse(String response) {
                try {
                    //JSONObject est un objet pour manipuler la réponse de PHP (en JSON)
                    JSONObject jsonObject = new JSONObject(response);

                    //getString('clé') getInt('clé') getJSONArray('clé')
                    // se sont des méthodes pour avoir les valeurs de JSON à partir de la clé
                    String success = jsonObject.getString("success");
                    JSONArray jsonArray=jsonObject.getJSONArray("login");

                    if (success.equals("1")){
                        for (int i=0; i<jsonArray.length();i++){

                            //Intent pour partir d'une activity(actuelle) à une autre activity
                            Intent intent=new Intent(MainActivity.this, Dashbord.class);
                            startActivity(intent);

                            Toast.makeText(MainActivity.this,"Success Login"  ,Toast.LENGTH_SHORT).show();
                            Loading.setVisibility(View.GONE);
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "Nom d'utilisateur ou mot de passe invalide", Toast.LENGTH_SHORT).show();
                        Loading.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Loading.setVisibility(View.GONE);
                    btn1.setVisibility(View.VISIBLE);
                    Toast.makeText(MainActivity.this,"ERROR1"+e.toString(),Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener() {

            //Si le PHP ne fontionne pas on execute onErrorResponse
            @Override
            public void onErrorResponse(VolleyError error) {
                Loading.setVisibility(View.GONE);
                btn1.setVisibility(View.VISIBLE);
                Toast.makeText(MainActivity.this,"ERROR"+error.toString(),Toast.LENGTH_LONG).show();

            }
        })
        {

            /*------------------------- POST seulement (mouch mawjouda fel GET) ---------------------*/
            //Création des parametres de la requete HTTP (POST) dans "params"
            //n3adiw les parametres POST
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Map object JAVA fih un tableau (clé,valeur)
                Map<String,String> params=new HashMap<>();
                params.put("username",username);
                params.put("password",password);

                // MAP: params = [("username", "imen"), ("password",1998)] / JSON:{"username": "imen", "password": 1998}
                return params;
            }
        };


        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}