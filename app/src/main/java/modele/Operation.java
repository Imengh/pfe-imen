package modele;

public class Operation {
    private String quantite;
    private String numero;

    public Operation() {
    }

    public Operation(String quantite) {
        this.quantite = quantite;
    }

    public Operation(String quantite, String numero) {
        this.quantite = quantite;
        this.numero = numero;
    }

    public String getQuantite() {
        return quantite;
    }

    public void setQuantite(String quantite) {
        this.quantite = quantite;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
