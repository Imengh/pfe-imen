package modele;

public class Controleuse {
    private String ctrlLib;
    private String ctrlId;


    public Controleuse() {
    }

    public Controleuse(String ctrlLib, String ctrlId) {
        this.ctrlLib = ctrlLib;
        this.ctrlId = ctrlId;
    }

    public String getCtrlLib() {
        return ctrlLib;
    }

    public void setCtrlLib(String ctrlLib) {
        this.ctrlLib = ctrlLib;
    }

    public String getCtrlId() {
        return ctrlId;
    }

    public void setCtrlId(String ctrlId) {
        this.ctrlId = ctrlId;
    }
}
