package modele;

public class HoraireTravail {
    private String dureeEnMinutes;

    public HoraireTravail() {
    }

    public HoraireTravail(String dureeEnMinutes) {
        this.dureeEnMinutes = dureeEnMinutes;
    }

    public String getDureeEnMinutes() {
        return dureeEnMinutes;
    }

    public void setDureeEnMinutes(String dureeEnMinutes) {
        this.dureeEnMinutes = dureeEnMinutes;
    }
}
