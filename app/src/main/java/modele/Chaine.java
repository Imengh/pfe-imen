package modele;

public class Chaine {
    private String id;

    public Chaine() {
    }

    public Chaine(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
