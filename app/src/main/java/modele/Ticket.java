package modele;

public class Ticket {

    private String numero;
    private Employe employe;
    private PlageHoraire plageHoraire;
    private Chaine chaine;

    public Ticket() {
    }

    public Ticket(String numero, Employe employe, PlageHoraire plageHoraire, Chaine chaine) {
        this.numero = numero;
        this.employe = employe;
        this.plageHoraire = plageHoraire;
        this.chaine = chaine;
    }

    public String getParams() {
        return "?suiveuse=" + this.numero + "&empId=" + this.employe.getMatricule() + "&chId=" + chaine.getId() + "&PlageH_Id=" + this.plageHoraire.getId() + "&Duree=" + this.plageHoraire.getHoraireTravail().getDureeEnMinutes();
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public PlageHoraire getPlageHoraire() {
        return plageHoraire;
    }

    public void setPlageHoraire(PlageHoraire plageHoraire) {
        this.plageHoraire = plageHoraire;
    }

    public Chaine getChaine() {
        return chaine;
    }

    public void setChaine(Chaine chaine) {
        this.chaine = chaine;
    }
}
