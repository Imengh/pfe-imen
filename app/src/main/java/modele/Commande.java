package modele;

public class Commande {
    private String numero;

    public Commande(String numero) {
        this.numero = numero;
    }

    public Commande() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
