package modele;

public class Paquet {
    private String numero;
    private String quantite;

    public Paquet() {
    }

    public Paquet(String numero, String quantite) {
        this.numero = numero;
        this.quantite = quantite;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getQuantite() {
        return quantite;
    }

    public void setQuantite(String quantite) {
        this.quantite = quantite;
    }
}
