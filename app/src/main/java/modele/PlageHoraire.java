package modele;

public class PlageHoraire {
    private String id;
    private HoraireTravail horaireTravail;

    public PlageHoraire() {
    }

    public PlageHoraire(String id, HoraireTravail horaireTravail) {
        this.id = id;
        this.horaireTravail = horaireTravail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public HoraireTravail getHoraireTravail() {
        return horaireTravail;
    }

    public void setHoraireTravail(HoraireTravail horaireTravail) {
        this.horaireTravail = horaireTravail;
    }
}
